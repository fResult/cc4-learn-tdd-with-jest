// 1. Design and Write Tests
// 2. Implement Function
const search = require("./search");

function test(title, callback) {
  try {
    callback();
    console.log(`${title}: ✓ test passed!`)
  } catch (ex) {
    console.error(ex.message);
    console.log(`${title}: ✕ test failed!`)
  }
}

function expect(actualResult) {
  return {
    toBe(expectedResult) {
      if (actualResult !== expectedResult)
        throw new Error(`${actualResult} is not equal to ${expectedResult}`)
    }
  }
}

// TODO: Write tests
test('When search 6 must return -1', () => expect(search([1,2,3,4,5], 6)).toBe(-1));

test('When search 3 must return 2', () => expect(search([1,2,3,4,5], 3)).toBe(2));

test('When search 1 must return 0', () => expect(search([1,2,3,4,5], 1)).toBe(0));

test('When search 1 must return 5', () => expect(search([1,2,3,4,5], 5)).toBe(4));
