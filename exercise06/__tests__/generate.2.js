import generateMinMaxAverage from "../generate"
import * as utils from "../utils"

test('returns max min average values', () => {
  // TODO: jest.fn(), mockReturnValueOnce
  const originalFuntion = utils.fetchNumberFromAPI;
  utils.fetchNumberFromAPI = jest.fn();

  utils.fetchNumberFromAPI
    .mockReturnValueOnce(2)
    .mockReturnValueOnce(5)
    .mockReturnValueOnce(6);
  const expected = { min: 2, max: 6, avg: (2 + 5 + 6) / 3 };

  const result = generateMinMaxAverage();

  expect(result).toEqual(expected);
  expect(utils.fetchNumberFromAPI).toHaveBeenCalledTimes(3);

  //reset
  utils.fetchNumberFromAPI = originalFuntion;
});