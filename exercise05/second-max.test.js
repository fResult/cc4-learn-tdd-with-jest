// 1. TDD -> Write tests, Implement, Refactor
const findSecondMax = require("./second-max");

const {} = require('jest');
// TODO: Write tests
test("When input [1, 2, 3, 4] must return 3", () => {
  expect(findSecondMax([1, 2, 3, 4])).toBe(3);
});

test("When input [5, 4, 3, 2] must return 3", () => {
  expect(findSecondMax([5, 4, 3, 2])).toBe(4);
});

test("When input [9, 7, 8, 6] must return 3", () => {
  expect(findSecondMax([9, 7, 8, 6])).toBe(8);
});

test("When input [5, 2, 7, 1] must return 3", () => {
  expect(findSecondMax([5, 2, 7, 1])).toBe(5);
});

test("When input [9999999] must throw error 'Can't find the number'", () => {
  expect(() => findSecondMax([9999999])).toThrow();
});

test("When input [] must throw error 'Can't find the number'", () => {
  expect(() => findSecondMax([])).toThrow();
});

test("When input [2, 2] must throw error 'Can't find the number'", () => {
  expect(() => findSecondMax([2, 2])).toThrow();
});

test("When input [5, 5, 5, 5] must throw error 'Can't find the number'", () => {
  expect(() => findSecondMax([5, 5, 5, 5])).toThrow();
});
