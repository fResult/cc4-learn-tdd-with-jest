function findSecondMax(arr) {
  // TODO: Implement Code
  let max = arr[0], secondMax = Number.MIN_SAFE_INTEGER;
  let maxIdx = -1;
  let isEqualNumbers;

  if (arr.length <= 1) {
    throw new Error("Can't find the number");
  } else if (arr.length > 1) {
    isEqualNumbers = arr.every(item => arr[0] === item);
    if (isEqualNumbers) throw  new Error("Can't find the number");
    arr.forEach((item, idx) => {
      if (max < item) {
        max = item;
        maxIdx = idx;
      }
    });

    if (maxIdx === -1) maxIdx = 0;

    arr.splice(maxIdx, 1);

    arr.forEach((item) => {
      if (secondMax < item) {
        secondMax = item;
      }
    });

    return secondMax;
  }

}

module.exports = findSecondMax;
