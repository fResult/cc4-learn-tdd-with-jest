function evaluate(expr) {
  // TODO: Implement Code
  expr = expr.split(' ');

  // if (expr.length === 1) {
  //   return parseInt(expr[0])
  // } else {
  //   if (expr[1] === '+') return evaluate(parseInt(expr[0]) + parseInt(expr[2]) + '');
  //   else return evaluate(parseInt(expr[0]) + parseInt(expr[2]));
  // }

  let result = parseInt(expr[0]);
  for (let i = 1; i < expr.length; i+=2) {
    if (expr[i] === '+') {
      result += parseInt(expr[i + 1]);
    } else {
      result -= parseInt(expr[i + 1]);
    }
  }

  return result
}

module.exports = evaluate;
