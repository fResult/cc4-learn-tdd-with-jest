// 1. TDD -> Write tests, Implement, Refactor
const evaluate = require("./eval");

const {} = require('jest');
// TODO: Write tests
test("When input '1' must return 1", () => {
  expect(evaluate("1")).toBe(1);
});

test("When input '1 + 1' must return 2", () => {
  expect(evaluate("1 + 1")).toBe(2);
});

test("When input '2 + 2' must return 2", () => {
  expect(evaluate("1 + 1")).toBe(2);
});

test("When input '9 - 5' must return 4", () => {
  expect(evaluate("9 - 5")).toBe(4);
});

test("When input '10 + 20' must return 30", () => {
  expect(evaluate("10 + 20")).toBe(30);
});

// test("When input '14' must return 14", () => {
//   expect(evaluate("14")).toBe(14);
// });

test("When input '145 - 155' must return -10", () => {
  expect(evaluate("145 - 155")).toBe(-10);
});

test("When input '1 + 1 + 1' must return 3", () => {
  expect(evaluate("1 + 1 + 1")).toBe(3);
});

test("When input '1 - 1 - 1' must return 3", () => {
  expect(evaluate("1 - 1 - 1")).toBe(-1);
});
// test("When input '11 + 12 - 13' must return 3", () => {
//   expect(evaluate("11 + 12 - 13")).toBe(-1);
// });

test("When input '1 - 2 + 3 - 4 + 5 - 6 + 7' must return 3", () => {
  expect(evaluate("1 - 2 + 3 - 4 + 5 - 6 + 7")).toBe(4);
});
