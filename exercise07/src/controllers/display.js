import db from "../lowdb";

async function displayTask(req, res) {
  const tasks = await db.get('tasks').value();
  if (tasks.length === 0) {
    res.send('You have no tasks');
  } else {
    let result = '';
    tasks.map((task, idx) => {
      result += `${idx + 1}. ${task['title']}<br>`;
      return result;
    });
    res.set('Content-Type', 'text/html');
    res.send(result);
  }
}

export { displayTask };