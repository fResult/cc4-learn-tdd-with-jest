// 1. Writing Tests
// 2. Implement Equal Function
// 3. Implement Code

const sortNumber = require("./sortNumber");

let result, expected;

// a > b > c false
result = sortNumber(3, 2, 1);
expected = [1, 2, 3];
if (!(expected[0] === result[0] && expected[1] === result[1] && expected[2] === result[2])) {
  throw new Error(`❌ ❌ ❌ Result: ${result} is not equal to Expected: ${expected}`);
} else console.log('✅ ✅ ✅', result, expected);


// b > c > a false
expected = [1, 3, 5];
result = sortNumber(1, 5, 3);
if (!(expected[0] === result[0] && expected[1] === result[1] && expected[2] === result[2])) {
  throw new Error(`❌ ❌ ❌ Result: ${result} is not equal to Expected: ${expected}`);
} else console.log('✅ ✅ ✅', expected, result);

// b == a < c false
expected = [3, 3, 5];
result = sortNumber(3, 3, 5);
if (!(expected[0] === result[0] && expected[1] === result[1] && expected[2] === result[2])) {
  throw new Error(`❌ ❌ ❌ Result: ${result} is not equal to Expected: ${expected}`);
} else console.log('✅ ✅ ✅', expected, result);

// b == a > c false
expected = [1, 3, 3];
result = sortNumber(3, 3, 1);
if (!(expected[0] === result[0] && expected[1] === result[1] && expected[2] === result[2])) {
  throw new Error(`❌ ❌ ❌ Expected: ${expected} is not equal to Result: ${result}`);
} else console.log('✅ ✅ ✅', expected, result);

// a == b == c false
expected = [3, 3, 3];
result = sortNumber(3, 3, 3);
if (!(expected[0] === result[0] && expected[1] === result[1] && expected[2] === result[2])) {
  throw new Error(`❌ ❌ ❌ Expected: ${expected} is not equal to Result: ${result}`);
} else console.log('✅ ✅ ✅', expected, result);

// c == a < b false
expected = [3, 3, 5];
result = sortNumber(3, 5, 3);
if (!(expected[0] === result[0] && expected[1] === result[1] && expected[2] === result[2])) {
  throw new Error(`❌ ❌ ❌ Expected: ${expected} is not equal to Result: ${result}`);
} else console.log('✅ ✅ ✅', expected, result);

// a == c > b false
expected = [1, 3, 3];
result = sortNumber(3, 1, 3);
if (!(expected[0] === result[0] && expected[1] === result[1] && expected[2] === result[2])) {
  throw new Error(`❌ ❌ ❌ Expected: ${expected} is not equal to Result: ${result}`);
} else console.log('✅ ✅ ✅', expected, result);

// b > a > c false
expected = [1, 3, 5];
result = sortNumber(3, 5, 1);
if (!(expected[0] === result[0] && expected[1] === result[1] && expected[2] === result[2])) {
  throw new Error(`❌ ❌ ❌ Expected: ${expected} is not equal to Result: ${result}`);
} else console.log(`✅ ✅ ✅ Expected: ${expected} is equal to Result: ${result}`);

// c < a < b false
expected = [1, 3, 3];
result = sortNumber(3, 5, 1);

// b < a < c false
expected = [1, 3, 3];
result = sortNumber(3, 1, 5);

// b > a > c false
expected = [1, 3, 3];
result = sortNumber(5, 1, 3);

// b < c < a false
expected = [1, 3, 3];
result = sortNumber(1, 5, 3);
