function sortNumber(a, b, c) {
  // TODO: Implement Function
  let temp;
  let result = [];

  if (a > b && b > c) {
    console.log('a > b && b > c');
    temp = a;
    a = c;
    c = temp;
  } else if (a < b && b > a) {
    console.log('a < b && b > a');
    temp = c;
    c = b;
    b = temp;
  } else if (b === a && a > c && b > c) {
    console.log('b === a && a > c && b > c');
    temp = c;
    c = a;
    a = temp;
  } else if (a === c && a > b && b < c) {
    console.log('a === c && a > b && b < c');
    temp = b;
    b = a;
    a = temp;
  } else if (b > a && a > c) {
    console.log('b > a && a > c');
    temp = c;
    c = b;
    b = a;
    a = temp;
  }

  result = pushToArray(a, b, c);

  return result;
}

function pushToArray(a, b, c) {
  const array = [];
  array.push(a);
  array.push(b);
  array.push(c);
  return array;
}

module.exports = sortNumber;
